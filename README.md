# DEP 0 Index of Devinstaller Enhancement Proposals

| DEP           | 0                                           |
| ------------- | ------------------------------------------- |
| Title         | Index of Devinstaller Enhancement Proposals |
| Author        | @justinekizhak                              |
| Status        | Draft                                       |
| Type          | Informational                               |
| Created       | 26-June-2020                                |
| Last-Modified | 27-June-2020                                |

[[_TOC_]]

# Categories

1.  Meta
2.  Dev
3.  Info

# Index

| DEP                        | DEP Title                  | Category | DEP Status |
| -------------------------- | -------------------------- | -------- | ---------- |
| [0001](dep-0001/README.md) | Housing and format of DEPs | Meta     | Draft      |
| [0002](dep-0002/README.md) | Commit message format      | Dev      | Draft      |
