# DEP 2 Commit message format

| DEP           | 2                     |
| ------------- | --------------------- |
| Title         | Commit message format |
| Author        | @justinekizhak        |
| Status        | Draft                 |
| Type          | Dev                   |
| Created       | 2-July-2020           |
| Last-Modified | 15-July-2020          |

[[_TOC_]]

# Abstract

The DEP 2 specification is a comprehensive and opinionated guide for writing commit messages. It provides set of rules and guidelines on writing commit messages and making it easier for humans as well as automated tools for handling the commit messages.

This specification has been inspired by the Conventional Commits and borrows a lot of ideas from it but the commits following this specification may not necessarily be compatible with the Conventional commits specification.

# Rational

One of the key rational behind is to provide as much information as possible to the readers, in less words as possible. It also understands that in this persuit of _providing as much information as possible,_ the readers may find its language a bit foreign but this is a compromise this specification would accept.

## Considerations

There were few things taken into consideration for the designing of the specification. One of them is auto generation of CHANGELOG using the commit message.

This specification recognizes that commit messages can provide more than sufficient information for writing the CHANGELOG, but It would like to clarify that the commit messages play an important role in CHANGELOG but they alone shouldn&rsquo;t be used as the source of truth and the commit messages shouldn&rsquo;t be added into the CHANGELOG as is.

# Specification

This specification starts by addressing the anatomy of commit messages.

This specification would define and allocate specific purpose to each parts of the commit messages.

## Commit message anatomy

The generally accepted commit message structure consists of two parts:

1.  Subject

    Generally the subject is expected to be a short summary of the code changes

2.  Body

    The body should be an elaborated description of the code changes.

This specification tries to define much more granular structure of a commit message.

```bnf
<message> ::= <subject> <body>

<subject> ::= <action>
            | <action> ": " <summary>
            | <action> "(" <scope> ")"
            | <action> "(" <scope> ")" ": " <summary>
            | "[" <type> "]" <action> ": " <summary>
            | "[" <type> "]" <action> "(" <scope> ")" ": " <summary>

<type> ::=  "feat"
          | "bug"
          | "perf"
<action> ::= "add"
          | "remove"
          | "update "
          | "migrate "
          | "fix"
          | "bump"
          | "rename"
          | "refactor"
          | "improve"
<scope> ::= word word?
<summary> ::= sentence

sentence ::= word+
word ::= characters+
<letter> ::= "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J"
           | "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" | "S" | "T"
           | "U" | "V" | "W" | "X" | "Y" | "Z" | "a" | "b" | "c" | "d"
           | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" | "n"
           | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x"
           | "y" | "z"

```

![img](commit_anatomy.png)

## Regex

```
^(?:\[(feat|bug|perf)\] )?(add|remove|update|migrate|improve|rename|bump|fix|refactor|merge)(?:\(([a-z0-9 ]*)\))?(?:: ([`A-Z#0-9].*[^.]))?$
```

![img](./regex_viz.png "Visual representation of the regex")

[Online regex playground](https://regex101.com/r/TqiGUj/6)

## Format

The commit message should be structured as follows:

```
[TYPE] ACTION(SCOPE): SUMMARY

CONTENT

FOOTER
```

|     | Name    | Type             | Optional | Description                       | Part of |
| --- | ------- | ---------------- | -------- | --------------------------------- | ------- |
| 1   | TYPE    | Noun             | Yes      | Describing the type of the commit | Subject |
| 2   | ACTION  | Verb             | No       | The action this commit takes      | Subject |
| 3   | SCOPE   | Noun             | Yes      | The scope of the commit           | Subject |
| 4   | SUMMARY | Starts with noun | Yes      | The summary of the commit         | Subject |
| 5   | CONTENT | TEXT             | Yes      | Provides additional information   | Body    |
| 6   | FOOTER  | TEXT             | Yes      | Provides space for highlighting   | Body    |

## Visibility

`visibility` tells which commits are meant to be tracked. Commits with `public` visibility will be tracked for monitoring the SDLC but commits with `private` won&rsquo;t be tracked for the monitoring of SDLC.

### Public

If the commit `subject` has a `type` then that commit is explicitly assumed to be `public`.

Also all public commits need a `summary`.

### Private

All commits are `private` by default, unless otherwise.

## Commit Subject

Try to keep the commit subject in less that 50 characters as possible

### Type

**OPTIONAL**

These are enclosed in square brackets.

List of `type`

1.  `feat`
2.  `bug`
3.  `perf`

4.  `feat`

    | Commit actions available |
    | ------------------------ |
    | `add`                    |
    | `remove`                 |

5.  `bug`

    | Commit actions available |
    | ------------------------ |
    | `fix`                    |

6.  `perf`

    | Commit actions available |
    | ------------------------ |
    | `improve`                |

### Action

**REQUIRED**

This describes what the commit action is.

All commit actions should describe what the main motivation behind it is instead of relying on their literal meaning.

- `add` Allowed for public commit. This action is your regular &ldquo;adds&rdquo; a new feature, file, function, class, module and package etc
- `remove` Allowed for public commit. This action is your regular &ldquo;removes&rdquo; a feature, file, function, class, module and packages etc
- `fix` Allowed for public commit. This is used exclusively for bug fixes. This action can only be used if the _Type_ of the commit is `bug`. This action is not allowed for any other purpose.
- `improve` Allowed for public commit. This commit is used exclusively for performance improvements. This action can only be used if the _Type_ of the commit is `perf`. This action is not allowed for any other purpose.
- `update` Not allowed for public commit. This action &ldquo;updates&rdquo;. This action is also used when external dependencies are upgraded.
- `migrate` Not allowed for public commit. This action trades in a feature, function, class, module, package for another
- `bump` Not allowed for public commit. This action is used for bumping up the version of the application as well as bumping up the version of its dependencies.
- `rename` Not allowed for public commit. This is used exclusively for file renaming.
- `refactor` Not allowed for public commit. This action is used for code refactoring.
- `merge` Not allowed for public commit. This action is used for merging branches

### Scope

**OPTIONAL**

This tells the scope of the commit. The Scope can anything the developer wishes to describe.

### Summary

**OPTIONAL**

Starts with a noun with the first alphabet capitalized. Message can&rsquo;t end with `.`

## Commit Body

The commit message body of `public` commits maybe used for public CHANGELOG, so appropriate care must be taken on how they should be worded.

But in all cases try to make the commit summary be as technical as possible.

### Content

**OPTIONAL**

### Footer

**OPTIONAL**

# Rejected ideas

# Reference Implementation

This specification is meant to be a guideline and it will not be providing a concrete implementation. But it will provide links of examples where it has been implemented.

- <https://gitlab.com/devinstaller/devinstaller/snippets/1993432>
