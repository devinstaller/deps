# DEP 1 &#x2013; Housing and format of DEPs

| DEP           | 1                          |
| ------------- | -------------------------- |
| Title         | Housing and format of DEPs |
| Author        | @justinekizhak             |
| Status        | Draft                      |
| Type          | Meta                       |
| Created       | 26-June-2020               |
| Last-Modified | 15-July-2020               |

[[_TOC_]]

# Folder structure

    ..
    ├── README.md
    ├── dep-0001
    │   └── README.md
    └── dep-0002
        └── README.md

    2 directories, 3 files
